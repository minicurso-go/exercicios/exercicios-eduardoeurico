package main

import "fmt"

func ex3() {

	for i := 1; i <= 100; i++ {
		if i%15 == 0 {
			fmt.Println("FizzBuzz")
		}
		if i%3 == 0 {
			fmt.Println("Fizz")
		}
		if i%5 == 0 {
			fmt.Println("Buzz")
		}

		if i%3 != 0 && i%5 != 0 && i%15 != 0 {
			fmt.Println(i)
		}

	}
}

func main() {
	ex3()
}
