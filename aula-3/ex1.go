package main

import "fmt"

func ex1() {
	for i := 0; i <= 30; i += 3 {

		fmt.Println(i)
	}
}

func main() {
	ex1()
}
