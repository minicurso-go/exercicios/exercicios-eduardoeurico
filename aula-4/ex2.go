package main

import "fmt"

func temp() float32 {
	var f float32
	x := runX("Deseja fazer de celsius para fahrenheit ou de fahrenheit para celsius: ")
	y := runY("O valor da escala que deseja converter: ")
	if x == 1 {
		f = (y * 1.8) + 32
	}
	if x == 2 {
		f = (y - 32) / 1.8
	}

	return f
}
func menu() {
	fmt.Println("=====MENU=====")
	fmt.Println("1.Celsius -> Fahrenheit")
	fmt.Println("2.Fahrenheit -> Celsius")
}
func main() {
	menu()
	fmt.Println(temp())
}
