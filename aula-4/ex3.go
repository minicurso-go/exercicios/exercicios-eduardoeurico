package main

import "fmt"

func listar() float32 {

	lista := [5]float32{}

	for i := 0; i < len(lista); i++ {
		fmt.Println("Números da lista: ")
		fmt.Scan(&lista[i])

	}
	media := (lista[0] + lista[1] + lista[2] + lista[3] + lista[4]) / 5
	return media
}

func main() {
	fmt.Println("O valor da média é ", listar())
}
