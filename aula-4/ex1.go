package main

import "fmt"

func somar() float32 {
	x := runX("Escolha o primeiro valor ")
	y := runY("Escolha o segundo valor ")
	soma := x + y
	return soma

}

func main() {
	fmt.Println(somar())
}
