package main

import "fmt"

func runx() float32 {
	var x float32

	fmt.Printf("Escolha o valor de X: ")
	fmt.Scan(&x)

	return x
}
func runy() float32 {

	var y float32
	fmt.Printf("Escolha o valor de Y: ")
	fmt.Scan(&y)

	return y
}
func reso() {
	x := runx()
	y := runy()
	fmt.Printf("O ponto está no ")
	if x > 0 && y > 0 {
		fmt.Printf("Quadrante 1")
	}
	if x < 0 && y > 0 {
		fmt.Printf("Quadrante 2")
	}
	if x < 0 && y < 0 {
		fmt.Printf("Quadrante 3")
	}
	if x > 0 && y < 0 {
		fmt.Printf("Quadrante 4")
	}
	if x == 0 && y == 0 {
		fmt.Printf("Origem")
	}
	if x == 0 && y != 0 {
		fmt.Printf("Eixo Y")
	}
	if x != 0 && y == 0 {
		fmt.Printf("Eixo X")
	}
	return
}

func main() {
	reso()

}
