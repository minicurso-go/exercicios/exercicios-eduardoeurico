package main

import "fmt"

func geometria() {
	x := runX("Escolha o valor de A: ")
	y := runY("Escolha o valor de B: ")
	z := runZ("Escolha o valor de C: ")

	areaTri := x * z
	areaCirc := (z * z) * 3.14159
	areaTra := ((x * y) * z) / 2
	areaQua := y * y
	areaRet := x * y

	fmt.Println(areaTri)
	fmt.Println(areaCirc)
	fmt.Println(areaTra)
	fmt.Println(areaQua)
	fmt.Println(areaRet)
}
func main() {
	geometria()
}
