package main

func somar() float32 {
	var soma float32
	x := runX("Escolha o 1º valor")
	y := runY("Escolha o 2º valor ")

	soma = x + y
	return soma
}
func subtrair() float32 {
	var sub float32
	x := runX("Escolha o 1º valor")
	y := runY("Escolha o 2º valor ")

	sub = x - y

	return sub
}
func multiplicar() float32 {
	var mult float32
	x := runX("Escolha o 1º valor")
	y := runY("Escolha o 2º valor ")
	mult = x * y
	return mult
}
func dividir() float32 {
	var div float32
	x := runX("Escolha o 1º valor")
	y := runY("Escolha o 2º valor ")
	div = x / y
	return div
}
