package main

import "fmt"

func dif() {
	x := runX("Valor de A: ")
	y := runY("Valor de B: ")
	z := runZ("Valor de C: ")
	w := runW("Valor de D: ")

	diferenca := (x * y) - (z * w)

	fmt.Println("A diferença é de ", diferenca)
}

func main() {
	dif()
}
